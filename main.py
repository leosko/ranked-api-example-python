import requests
import demjson

UNIT_TYPE_CREATE = 4
UNIT_TYPE_DESTROY = 5

unitTypeToName = demjson.decode('{0:"dagger",1:"mace",2:"fortress",3:"scepter",4:"reign",5:"nova",6:"pulsar",7:"quasar",'
                            '8:"vela",9:"corvus",10:"crawler",11:"atrax",12:"spiroct",13:"arkyid",14:"toxopid",'
                            '15:"flare",16:"horizon",17:"zenith",18:"antumbra",19:"eclipse",20:"mono",21:"poly",'
                            '22:"mega",23:"quad",24:"oct",25:"risso",26:"minke",27:"bryde",28:"sei",29:"omura",'
                            '30:"alpha",31:"beta",32:"gamma",33:"block"}')


def get_match_info(match_id):
    return requests.get(f'https://ranked.ddns.net/api/match?id={match_id}').json()


def get_match_units(match_id):
    return requests.get(f'https://ranked.ddns.net/api/buildOrder?id={match_id}&parse=true&types=4,5&aggregate=true').json()


created_units = dict()
destroyed_units = dict()
for i in range(32819, 32900):
    match_units = get_match_units(i)

    for event in match_units:
        if event == "error" or len(event) == 0:
            continue
        if event["type"] == UNIT_TYPE_CREATE:
            if event["unitType"] in created_units:
                created_units[event["unitType"]] += event["count"]
            else:
                created_units[event["unitType"]] = event["count"]
        elif event["type"] == UNIT_TYPE_DESTROY:
            if event["unitType"] in destroyed_units:
                destroyed_units[event["unitType"]] += event["count"]
            else:
                destroyed_units[event["unitType"]] = event["count"]
    print(i)

for i in created_units:
    print(f'{unitTypeToName[i]}: {created_units[i]}')
for i in destroyed_units:
    print(f'{unitTypeToName[i]}: {destroyed_units[i]}')